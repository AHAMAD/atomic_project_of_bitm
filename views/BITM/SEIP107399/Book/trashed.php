<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'BITM'.DIRECTORY_SEPARATOR.'SEIP107399'.DIRECTORY_SEPARATOR.'start_up.php'); 
use App\Bitm\SEIP107399\Book\book;
use App\Bitm\SEIP107399\Utility\Utility;
use App\Bitm\SEIP107399\Message\Message;
$book = new book();
$books=$book->trashed(); 
//Utility::d($books);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project</title>

    <!-- Bootstrap -->
    <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../resource/font_awesome/css/font-awesome.min.css" rel="stylesheet">
    
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <p><br></p>
      <div class="container">
          
          
          <nav class="navbar navbar-default">
  <div class="container-fluid">
      
    <div class="navbar-header">
      
      <a class="navbar-brand top" href="#">
          <img alt="Book" src="../../../../resource/image/book.gif" style="height: 80px; width: 200px;">
      </a>
    </div>
      
      <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search Book Here">
  </div>
  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Search</button>
</form>
  </div>
             
</nav>
          
          
          <center><h1 style="padding-right: 20px;">Trashed</h1></center>
          
          
          
         
              <ul class="nav nav-tabs"  >
  <li><a href="../../../../index.php">Home</a></li>
  <li><a href="index.php">List</a></li>
  <li><a href="create.php" >Create</a></li>
  <li class="active"><a href="trashed.php">Trashed</a></li>
</ul>
          
          
          
          
          <br><br>   
             
  <form action="recovermultiple.php" method="post">
<div class="tab-content">
    <div class="tab-pane active" id="home">
        <br>
        
        
        <!--row !-->
    <div class="row">
            
  <div class="col-xs-6 col-md-4"> 
      
      
     
    <button class="btn bg-success">
        <span>
                   Show
                   &nbsp;
                   <select class="btn">
                       <option class="btn">10</option>
                       <option class="btn">20</option>
                       <option class="btn">30</option>
                       <option class="btn">40</option>
          </select>
                      &nbsp;
                   entries
                </span>
    </button>
          
      
  
  </div>
  
  <div class="col-xs-6 col-md-4">
  
    
          
           <div class="col-xs-3 col-md-2">
               &nbsp;&nbsp;
               </div>
      
      
      <div class="col-xs-3 col-md-2">
            &nbsp;&nbsp;
          </div>
      
      
       <div class="col-xs-3 col-md-2">
            <button class="btn btn-default" title="Download as pdf">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i>
    <span class="caret"></span>
                </a>
            </button>
               </div>
      
      
      <div class="col-xs-3 col-md-2">
           <button class="btn btn-default" title="Download as excel">
               <a href="#">
                   <i class="fa fa-file-excel-o"></i>
    <span class="caret"></span>
               </a>
           </button>
          </div>
      
   
 </div>
  
  <div class="col-xs-6 col-md-4">
      
      <button type="submit" class="btn bg-success">
              <span class="glyphicon glyphicon-duplicate">
                    Recover
              </span>
          </button>
      
      <button type="button" class="btn " id="deleteall" style="float: right;">
              <span class="glyphicon glyphicon-trash">
                    Delete
              </span>
          </button>
     
      
  </div>
            
</div>
        
        
        
        
        
        
                    <br>
                    
                    
                    
                    <center>
                        <div id="message" class="bg-success">
              
              <?php echo Message::display_message(); ?>
                       </div>
                    </center> 
                    <br>
                    
                        
                
                        
                          <div class="table-responsive">
                           
                         <table class="table table-hover/*table-bordered table-striped table-condensed*/">
                             <thead>
                                 <tr class="success">
                                     <th><input  type="checkbox" name="markall" id="markall" ></th>
                                   <th>Serial</th>
                                     <th>Id</th>
                                     <th><center>Book Title</center></th>
                                <th><center>Author</center></th>
                                     <th><center>Action</center></th>
                                     
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php
                                 if (count($books)>0){
                                 $serialno=1;
                                 foreach ($books as $book){
                                 ?>
                    <tr>
                        <td><input value="<?php echo $book['id']; ?>" type="checkbox" class="mark" name="mark[]" ></td>
                                    <td><?php echo $serialno; ?></td>
                                     <td><?php echo $book['id']; ?></td>
                                     <td><center><a href="show.php?id=<?php echo $book['id']; ?>"><?php echo $book['title']; ?></a></center></td>
                                   <td><center><?php echo $book['author']; ?></center></td>
                        <td>
                        
                        <center>
                              <a  href="recover.php?id=<?php echo $book['id']; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" title="View"> Recover </a>&nbsp;&nbsp;
                              <a  href="delete.php?id=<?php echo $book['id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-eye-open" title="View"> Delete </a>&nbsp;&nbsp;
                                
                              
                                  
                        </center>
                        </td>
                        
                    </tr>
                                 <?php 
                                 $serialno++;
                                 } 
                                 }  else {
                                     

                                 ?>
                    
                     <tr>
                         <td colspan="6"><center>No Record is Available</center></td>  
                     </tr>
                                 <?php } ?>
                             </tbody>
                             
                             
                             
                             
                             
                            
                         </table>
                                
                    </div>
                        
                        
                    
                        
                      
                        
                     
                        
                        
                        
                    
                    
                    <br>
                    
                         <ul class="pagination">
                             <li class="embed-responsive"><a href="../../../../index.php">&laquo;</a></li>
                             <li class="active"><a href="index.php">1</a></li>
                             <li><a href="create.php">2</a></li>
                               <li><a href="#">3</a></li>
                               <li><a href="#">4</a></li>
                               <li><a href="#">5</a></li>
                               <li><a href="index.php">&raquo;</a></li>
                        </ul>
                   
                    
                   
        
               </div>
    
                     <div class="tab-pane" id="profile">
                           <br>
        
                              
    </div>
  
</div>
      </form>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
        
        
      $(document).ready(function(){
          
          $('.btn-danger').bind('click',function(){
         var deleteitm =   confirm('Are you sure to delete id?');
         if(!deleteitm){
             return false;
         }
        });
         
        $('#message').hide(10000);
       
       $('#markall').bind('click',function(){
           if($('#markall').is(':checked')){
               $('.mark').each (function(){
                           this. checked=true;
               });
               
           }
           else{
                $('.mark').each (function(){
                           this. checked=false;
               });
               
           }
           
       });
       
       
        
          $('#deleteall').bind('click',function(){
         var deleteitm =   confirm('Are you sure to delete all items?');
         if(deleteitm){
             document.forms[0].action='deletemultiple.php';
             document.forms[0].submit();
         }
        });
          
      });
       //$('.alert alert-success fade in').hide(10000);
        
        </script>
    
    
    
     <script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>
    
   
  </body>
</html>