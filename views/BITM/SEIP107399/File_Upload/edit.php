
<?php
include_once '../start_up.php';
//include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP107399\File_Upload\File_Upload;
use App\Bitm\SEIP107399\Utility\Utility;
use App\Bitm\SEIP107399\Message\Message;

$fileupload=new File_Upload();
$files=$fileupload->show($_GET['id']);
//Utility::d($files);
?>








<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project of BITM</title>

    <!-- Bootstrap -->
    <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <p><br></p>
      <div class="container">
          <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      
      <a class="navbar-brand top" href="#">
          <img alt="Book" src="../../../../resource/image/company_logo.jpg" style="height: 80px; width: 200px;">
      </a>
    </div>
      
      <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search Book Here">
  </div>
  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Search</button>
</form>
      
      
      
      
      
  </div>
             
</nav>
          
          <center><h1 style="padding-right: 20px;">Add File</h1></center>
          
          
  
              <ul class="nav nav-tabs" role="tablist" >
  <li><a href="../../../../index.php" role="tab" data-toggle="tab">Home</a></li>
  <li><a href="index.php" role="tab" data-toggle="tab">List</a></li>
  <li  class="active"><a href="create.php" role="tab" data-toggle="tab">Create</a></li>
  
</ul>
     
          
          <br><br>
 
<div class="tab-content">
    <div class="tab-pane active" id="home">
        <br>
        <form role="form"  action="update.php" method="post" enctype="multipart/form-data">
  
            <input type="text" name="id" value="<?php echo $files->id;  ?>"
           <label for="name">Enter File Name</label>
    
            <input value="<?php echo $files->name;  ?>" required="required" tabindex="1" autofocus="autofocus" id="name" name="name" type="text" class="form-control" >
           <br>
           <br>
           <label for="filename">Enter File</label>
    
           <!--<input required="required" tabindex="2"  id="Summary" name="Summary" type="text" class="form-control" placeholder="Enter author Book Name ">-->
           <input type="file" required="required" tabindex="2"   name="filename"   />
    <br>
    <br>
    
    <button tabindex="3" type="submit" class="btn" name="submit">Save</button>
    <button tabindex="4" type="button" class="btn">Save & Add Again</button>
    <button tabindex="5" type="reset" class="btn">Reset</button>
 
</form>
        <br>
                    
                         <ul class="pagination">
                             <li class="embed-responsive"><a href="javascript:history.go(-1)">&laquo;</a></li>
                             <li class="active"><a href="index.php">Go To List Page</a></li>
                               
                               <li><a href="#">&raquo;</a></li>
                        </ul>
                   
                    
                   
        
               </div>
    
                     <div class="tab-pane" id="profile">
                           <br>
        
                              
    </div>
  
</div>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <!-- <script src="../../../../resource/bootstrap/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>