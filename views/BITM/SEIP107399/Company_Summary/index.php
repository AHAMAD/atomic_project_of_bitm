<?php
//include_once '../../../../vendor/autoload.php'; //using relative path
include_once '../start_up.php';
use App\Bitm\SEIP107399\Message\Message;
use App\Bitm\SEIP107399\Company_Summary\Company_Summary;
use App\Bitm\SEIP107399\Utility\Utility;
$Company=new Company_Summary();
$companies=$Company->index();

//Utility::d($companies);


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project</title>

    <!-- Bootstrap -->
    <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../resource/font_awesome/css/font-awesome.min.css" rel="stylesheet">
    
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <p><br></p>
      <div class="container">
          
          
          <nav class="navbar navbar-default">
  <div class="container-fluid">
      
    <div class="navbar-header">
      
      <a class="navbar-brand top" href="#">
          <img alt="Book" src="../../../../resource/image/company_logo.jpg" style="height: 80px; width: 200px;">
      </a>
    </div>
      
      <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search Book Here">
  </div>
  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Search</button>
</form>
  </div>
             
</nav>
          
          
          <center><h1 style="padding-right: 20px;">Company Summary</h1></center>
          
         
              <ul class="nav nav-tabs" role="tablist" >
  <li><a href="../../../../index.php">Home</a></li>
  <li class="active"><a href="index.php">List</a></li>
  <li><a href="create.php">Create</a></li>
  
</ul>
          
          
          
          
          <br><br>   
 
<div class="tab-content">
    <div class="tab-pane active" id="home">
        <br>
        
        
        <!--row !-->
        <div class="row">
            
  <div class="col-xs-6 col-md-4"> 
      
      
     
    <button class="btn bg-success">
        <span>
                   Show
                   &nbsp;
                   <select class="btn">
                       <option class="btn">10</option>
                       <option class="btn">20</option>
                       <option class="btn">30</option>
                       <option class="btn">40</option>
          </select>
                      &nbsp;
                   entries
                </span>
    </button>
          
      
  
  </div>
  
  <div class="col-xs-6 col-md-4">
  
    
          
           <div class="col-xs-3 col-md-2">
               &nbsp;&nbsp;
               </div>
      
      
      <div class="col-xs-3 col-md-2">
            &nbsp;&nbsp;
          </div>
      
      
       <div class="col-xs-3 col-md-2">
            <button class="btn btn-default" title="Download as pdf">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i>
    <span class="caret"></span>
                </a>
            </button>
               </div>
      
      
      <div class="col-xs-3 col-md-2">
           <button class="btn btn-default" title="Download as excel">
               <a href="#">
                   <i class="fa fa-file-excel-o"></i>
    <span class="caret"></span>
               </a>
           </button>
          </div>
      
   
 </div>
  
  <div class="col-xs-6 col-md-4">
      <a href="create.php" style="float: right;"> 
          <button class="btn bg-success">
              <span class="glyphicon glyphicon-plus">
                    Add New
              </span>
          </button>
      </a>
  </div>
            
</div>
        
        
        
        
        
        
                    <br>
                    <center> 
                        
                        <div id="message" class="bg-warning">
                        <?php echo Message::display_message(); ?>
                    </div>
                    
                    </center>
                   
                    
                    <br>      
                    
                    
                    <div class="table-responsive">
                         <table class="table table-hover/*table-bordered table-striped table-condensed*/">
                             <thead>
                                 <tr class="success">
                                     <th>Serial</th>
                                     <th>Id</th>
                                     <th><center>Company Name</center></th>
                                     <th><center>Company Summary</center></th>
                                     <th><center>Action</center></th>
                                    
                                     
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php 
                                 $sereil=1;
                                 foreach ($companies as $com){ 
                                     $rest = substr($com->Company_Summary,0, 40);
                                     $result=$rest.".....";
                                     
                                 
?>
                                 
                    <tr>
                                  <td><ecnter><?php echo $sereil; ?></ecnter></td>
                                     <td><?php echo $com->id; ?></td>
                                     <td><center><?php echo $com->Company_Name ;?></center></td>
                                     <td><center><?php echo $result ; ?></center></td>
                                    
                        <td>
                             <center>
                                 <a  href="show.php?id=<?php echo $com->id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" title="View"> View </a>&nbsp;&nbsp;
                                 <a href="edit.php?id=<?php echo $com->id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit" title="Edit"> Edit </a>&nbsp;&nbsp;
                                 
                                 <a href="#" class="btn btn-info"><span class="glyphicon glyphicon-export" title="Recover"> Recover </span> </a>&nbsp;&nbsp;
                                 <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-transfer" title="Email To Friend"> Email To Friend </span> </a>&nbsp;&nbsp;
                                  <form action="delete.php" method="post">
                                     <input type="hidden" name="id" value="<?php echo $com->id; ?>"/>
                                     <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash" title="Delete"> Delete </span></button>
                                <!-- <a href="delete.php?id=<?php echo $com->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash" title="Delete"> Delete </span></a>&nbsp;&nbsp;-->
                                 </form>   
                             </center>
                        </td>
                    </tr>
                             <?php  
                               
                                 } 
                                 $sereil++
                           
                             ?>    
                             </tbody>
                             
                             
                             
                             
                             
                            
                         </table>
                    </div>
                    <br>
                    
                         <ul class="pagination">
                             <li class="embed-responsive"><a href="../../../../index.php">&laquo;</a></li>
                             <li class="active"><a href="index.php">1</a></li>
                             <li><a href="create.php">2</a></li>
                               <li><a href="#">3</a></li>
                               <li><a href="#">4</a></li>
                               <li><a href="#">5</a></li>
                               <li><a href="index.php">&raquo;</a></li>
                        </ul>
                   
                    
                   
        
               </div>
    
                     <div class="tab-pane" id="profile">
                           <br>
        
                              
    </div>
  
</div>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <!--   <script src="../../../../resource/bootstrap/jQuery 1.11.3/jquery.min.js"></script>
   
   
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
    
    <script>
        $('.btn-danger').bind('click',function(){
            var deleteitm=confirm('Do you want to delete it?');
            if(!deleteitm){
                return false;
            }
        });
    $('#message').hide(10000);
    </script>
    
     <script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
    
</script>
   
    
  </body>
</html>