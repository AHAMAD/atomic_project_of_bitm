<?php
include_once '../start_up.php'; 
//include_once '../../../../vendor/autoload.php'; 
//include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'BITM'.DIRECTORY_SEPARATOR.'SEIP107399'.DIRECTORY_SEPARATOR.'start_up.php';
use App\Bitm\SEIP107399\email_subscription\email_subscription;
use App\Bitm\SEIP107399\Message\Message;
use App\Bitm\SEIP107399\Utility\Utility;

$email=new email_subscription();

$view=$email->show($_GET['id']);
//Utility::d($view);

/*echo $email->email=$_POST['Email'];
echo $email->email=$_POST['Password'];
echo $email->email=$_POST['Subscription_Date'];*/
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project</title>

    <!-- Bootstrap -->
    <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../resource/font_awesome/css/font-awesome.min.css" rel="stylesheet">
   
    
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <p><br></p>
      <div class="container">
          
          
          <nav class="navbar navbar-default">
  <div class="container-fluid">
      
    <div class="navbar-header">
      
      <a class="navbar-brand top" href="#">
          <img alt="Book" src="../../../../resource/image/email_edited.jpg" style="height: 80px; width: 200px;">
      </a>
    </div>
      
      <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search Book Here">
  </div>
  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Search</button>
</form>
  </div>
             
</nav>
          
          <center><h1 style="padding-right: 20px;">Email Address</h1></center>
          
          
              <ul class="nav nav-tabs" role="tablist" >
  <li><a href="../../../../index.php">Home</a></li>
  <li ><a href="index.php">List</a></li>
  <li><a href="create.php">Create</a></li>
  <li class="active"><a href="show.php">View</a></li>
</ul>
     
          
      <br><br>   
 
<div class="tab-content">
    <div class="tab-pane active" id="home">
        <br>
        
        
       
        <!--row !-->
        <div class="row">
            
  <div class="col-xs-6 col-md-4"> 
      
      
      
    <button class="btn bg-success">
        <span>
                   Show
                   &nbsp;
                   <select class="btn">
                       <option class="btn">10</option>
                       <option class="btn">20</option>
                       <option class="btn">30</option>
                       <option class="btn">40</option>
          </select>
                      &nbsp;
                   entries
                </span>
    </button>
          
        
  </div>
  
  <div class="col-xs-6 col-md-4">
  
    
          
           <div class="col-xs-3 col-md-2">
               &nbsp;&nbsp;
               </div>
      
      
      <div class="col-xs-3 col-md-2">
            &nbsp;&nbsp;
          </div>
      
      
       <div class="col-xs-3 col-md-2">
            <button class="btn btn-default" title="Download as pdf">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i>
    <span class="caret"></span>
                </a>
            </button>
               </div>
      
      
      <div class="col-xs-3 col-md-2">
           <button class="btn btn-default" title="Download as excel">
               <a href="#">
                   <i class="fa fa-file-excel-o"></i>
    <span class="caret"></span>
               </a>
           </button>
          </div>
      
   
 </div>
  
  <div class="col-xs-6 col-md-4">
      <a href="create.php" style="float: right;"> 
          <button class="btn bg-success">
              <span class="glyphicon glyphicon-plus">
                    Add New
              </span>
          </button>
      </a>
  </div>
            
</div>
        
        
        
        
        
        
        
        
        <br>
       
        
                    
                    
                    <br>      
                    
                    
                    <div class="table-responsive">
                         <table class="table table-hover/*table-bordered table-striped table-condensed*/">
                             <thead>
                                 <tr class="success">
                                     
                                     <th><center>Id</center></th>
                                     <th><center>Email</center></th>
                                     
                         <th><center>Date of Subscription</center></th>
                                     
                                     
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php
                                
                               
                                     $date=  Utility::change_date_formate($view['date_of_subscription'])
                                     
                                 ?>
                                 
                                 
                    <tr>
                       
                        <td><center><?php echo $view['id']; ?></center></td>
                         <td><center><?php echo $view['email']; ?></center></td>
                                     
                                     <td><center><?php echo $date; ?></center></td>
                        
                    </tr>
                              
                                 
                               
                             </tbody>
                             
                             
                             
                             
                             
                            
                         </table>
                    </div>
                    <br>
                    
                         <ul class="pagination">
                             <li class="embed-responsive"><a href="../../../../index.php">&laquo;</a></li>
                             <li class="active"><a href="index.php">1</a></li>
                             <li><a href="create.php">2</a></li>
                               <li><a href="#">3</a></li>
                               <li><a href="#">4</a></li>
                               <li><a href="#">5</a></li>
                               <li><a href="index.php">&raquo;</a></li>
                        </ul>
                   
                    
                   
        
               </div>
    
                     <div class="tab-pane" id="profile">
                           <br>
        
                              
    </div>
  
</div>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  
  
  
  <script src=" https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
    
     
    <!--<script>
     $('.btn-danger').bind('click',function(){
      var deleteMessage=confirm('Do you want to delete it?');  
      if(!deleteMessage){
          return false;
      }
     });
     
     $('#message').hide(10000);
        </script>-->

     <script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>
    
    
  </body>
</html>