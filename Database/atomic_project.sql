-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 09:57 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`) VALUES
(55, 'AHAMAD SHARIF', '2015-12-20'),
(56, 'AHAMAD SHARIF', '2015-12-20'),
(57, 'AHAMAD SHARIF', '2015-12-20'),
(58, '', '0000-00-00'),
(59, '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `author`, `deleted_at`) VALUES
(53, 'ahamad', 'phyhics', '1452633945'),
(54, 'AHAMAD SHARIF', 'Jason gilmore', '1452633949');

-- --------------------------------------------------------

--
-- Table structure for table `checkbox`
--

CREATE TABLE IF NOT EXISTS `checkbox` (
`id` int(11) NOT NULL,
  `football` varchar(55) NOT NULL,
  `cricket` varchar(55) NOT NULL,
  `badminton` varchar(55) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkbox`
--

INSERT INTO `checkbox` (`id`, `football`, `cricket`, `badminton`) VALUES
(9, 'football', 'cricket', 'badminton'),
(10, 'football', '', ''),
(11, '', 'cricket', ''),
(12, 'football', '', ''),
(13, '', '', 'badminton'),
(14, 'football', '', ''),
(15, '', 'cricket', ''),
(16, '', '', ''),
(17, 'football', 'cricket', 'badminton');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`) VALUES
(5, 'Sylhet'),
(6, 'Sylhet'),
(7, 'Chittagang'),
(8, 'Sylhet'),
(9, 'Chittagang'),
(10, 'Chittagang');

-- --------------------------------------------------------

--
-- Table structure for table `company_summary`
--

CREATE TABLE IF NOT EXISTS `company_summary` (
`id` int(11) NOT NULL,
  `Company_Name` varchar(255) NOT NULL,
  `Company_Summary` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_summary`
--

INSERT INTO `company_summary` (`id`, `Company_Name`, `Company_Summary`) VALUES
(13, 'BITM', 'To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology & Management (BITM) with the support of World Bank. BITM was established with a vision to be a world-class IT institute in Bangladesh for the purpose of enhancing the competitiveness of the IT Sector in Bangladesh by creating a pool of qualified IT professionals and quality certified IT companies.'),
(14, 'BASIS', 'Bangladesh Association of Software and Information Services (BASIS) is the national trade body for Software & IT Enabled Service industry of Bangladesh. Established in 1997, the association has been working with a vision of developing vibrant software & IT service industry in the country.');

-- --------------------------------------------------------

--
-- Table structure for table `email_subscription`
--

CREATE TABLE IF NOT EXISTS `email_subscription` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_of_subscription` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_subscription`
--

INSERT INTO `email_subscription` (`id`, `email`, `password`, `date_of_subscription`) VALUES
(10, 'najmulmktjnu4@gmail.com', '22222222222222', '2012-12-12'),
(12, 'monbromora@gmail.com', '1111', '2015-12-08'),
(13, 'monbromora@gmail.com', '1111', '2015-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `file_name`
--

CREATE TABLE IF NOT EXISTS `file_name` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_name`
--

INSERT INTO `file_name` (`id`, `name`, `file_name`) VALUES
(33, 'email', 'Uploads/email3.jpg'),
(37, 'Book', 'Uploads/book.jpg'),
(38, 'Birthday', 'Uploads/Birthday3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `male` varchar(255) NOT NULL,
  `female` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `male`, `female`) VALUES
(4, 'male', ''),
(5, 'male', ''),
(6, 'male', 'female'),
(7, '', 'female'),
(8, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `term_condition`
--

CREATE TABLE IF NOT EXISTS `term_condition` (
`id` int(7) NOT NULL,
  `agree` varchar(255) NOT NULL,
  `disagree` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `term_condition`
--

INSERT INTO `term_condition` (`id`, `agree`, `disagree`) VALUES
(3, '', 'agree'),
(6, '', 'disagree'),
(7, 'agree', ''),
(8, '', 'disagree');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkbox`
--
ALTER TABLE `checkbox`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_summary`
--
ALTER TABLE `company_summary`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_subscription`
--
ALTER TABLE `email_subscription`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_name`
--
ALTER TABLE `file_name`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `term_condition`
--
ALTER TABLE `term_condition`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `checkbox`
--
ALTER TABLE `checkbox`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `company_summary`
--
ALTER TABLE `company_summary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `email_subscription`
--
ALTER TABLE `email_subscription`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `file_name`
--
ALTER TABLE `file_name`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `term_condition`
--
ALTER TABLE `term_condition`
MODIFY `id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
