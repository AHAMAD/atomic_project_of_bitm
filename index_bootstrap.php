<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project of BITM</title>

    <!-- Bootstrap -->
    <link href="resource/bootstrap/css/bootstrap.css" rel="stylesheet">

    <link href="resource/font_awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
      <!--
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
          <img alt="Brand" src="resource/image/project.jpg">
      </a>
    </div>
  </div>
</nav>
   -->
   <nav class="navbar navbar-fixed-top bg-success">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#"><img alt="Brand" src="resource/image/project.jpg"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
     <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search Book Here">
  </div>
  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>Search</button>
</form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
   
   <br><br><br><br>
          
   
   
   <ul class="nav nav-tabs nav-pills  ">
        <li class=""><a href="#">Home<span class="sr-only">(current)</span></a></li>
        
        <li class="dropdown">
          <a href="views/BITM/SEIP107399/Book" target="_blank" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Book <span class=""></span></a>
          <ul class="dropdown-menu">
              
              <li class=""><a href="views/BITM/SEIP107399/Book/index.php" target="_blank">List</a></li>
           
              <li><a href="views/BITM/SEIP107399/Book/create.php" target="_blank">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
            <a href="views/BITM/SEIP107399/email_subscription/" target="_blank" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
              <li><a href="views/BITM/SEIP107399/email_subscription/index.php" target="_blank">List</a></li>
           
              <li><a href="views/BITM/SEIP107399/email_subscription/create.php" target="_blank">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="views/BITM/SEIP107399/Book" target="_blank" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Birthday <span class=""></span></a>
          <ul class="dropdown-menu">
              
              <li><a href="views/BITM/SEIP107399/Birthday/index.php" target="_blank">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="views/BITM/SEIP107399/Birthday/create.php" target="_blank">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Email <span class=""></span></a>
          <ul class="dropdown-menu">
              
            <li><a href="#">List</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Create</a></li>
          </ul>
        </li>
        
        
      </ul>
   
   
   
    <br>
    <div class="container">
        <center> <h1>ATOMIC PROJECT</h1>
                <h2>PHP WEB DEVELOPMENT</h2>
                <h2>AHAMAD SHARIF</h2>
                <h3>107399</h3>
                <h4>BATCH:10</h4>
               
        </center>
       
        <br>
         <br>
         
         
         
          <!--row !-->
        <div class="row">
            
  <div class="col-xs-6 col-md-4"> 
      
      
      
    <button class="btn bg-success">
        <span>
                   Show
                   &nbsp;
                   <select class="btn">
                       <option class="btn">10</option>
                       <option class="btn">20</option>
                       <option class="btn">30</option>
                       <option class="btn">40</option>
          </select>
                      &nbsp;
                   entries
                </span>
    </button>
          
        
  </div>
  
  <div class="col-xs-6 col-md-4">
  
    
          
           <div class="col-xs-3 col-md-2">
               &nbsp;&nbsp;
               </div>
      
      
      <div class="col-xs-3 col-md-2">
            &nbsp;&nbsp;
          </div>
      
      
       <div class="col-xs-3 col-md-2">
            <button class="btn btn-default" title="Download as pdf">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i>
    <span class="caret"></span>
                </a>
            </button>
               </div>
      
      
      <div class="col-xs-3 col-md-2">
           <button class="btn btn-default" title="Download as excel">
               <a href="#">
                   <i class="fa fa-file-excel-o"></i>
    <span class="caret"></span>
               </a>
           </button>
          </div>
      
   
 </div>
          
            
            
            
            
          <div class="col-xs-6 col-md-4"> 
              
              <div class="col-xs-3 col-md-2">
          
          </div>
              
       <div class="col-xs-3 col-md-2">
           
            
       </div>
          <div class="col-xs-3 col-md-2">
           
          
       </div>
          
           <div class="col-xs-3 col-md-2">
           
            
       </div>
             
              <div class="col-xs-3 col-md-2">
                  
                  
                 
     
          <button class="btn bg-success">
              <span class="">
              <li class="dropdown">
                  <a href="views/BITM/SEIP107399/Book" target="_blank" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" text-decoration: none;">Add New <span class="glyphicon glyphicon-plus"></span> </a>
          <ul class="dropdown-menu">
              
             
              <li class=""><a href="views/BITM/SEIP107399/Book/create.php" target="_blank" style="text-decoration:  none;">Book</a></li>
           
              <li><a href="views/BITM/SEIP107399/Birthday/create.php" target="_blank" style=" text-decoration: none;">Birthday</a></li>
              <li><a href="views/BITM/SEIP107399/email_subscription/create.php" target="_blank" style=" text-decoration: none;">Email</a></li>
          </ul>
        </li>
              </span>
              
          </button>
      
 
                  
                   
              </div>
      
   
        
  </div>
  
            
            
</div>
         
         
         
         
         <br>
         <br>
         
        <div class="table-responsive">
                         <table class="table table-hover/*table-bordered table-striped table-condensed*/">
                             <thead>
                                 <tr class="success">
                                     <th><center>Serial</center></th>
                                     <th><center>Project Name</center></th>
                                      
                                     
                                 </tr>
                             </thead>
                             <tbody>
                                
                                    
                                 <tr>
                                  <td><center>1</center></td>
                             <td><center><a href="views/BITM/SEIP107399/Book" target="_blank">Book Name</a></center></td>
                         
                             
                             </tr>
                             <tr>
                                  <td><center>2</center></td>
                                  <td><center><a href="views/BITM/SEIP107399/email_subscription/" target="_blank">Email</a></center></td>
                         
                             
                             </tr>
                             <tr>
                                  <td><center>3</center></td>
                                  <td><center><a href="views/BITM/SEIP107399/Birthday/" target="_blank">Birthday</a></center></td>
                         
                             
                             </tr>
                                 
                             </tbody>
                             
                             
                            
                         </table>
                    </div>
                    <br>
                    
                         <ul class="pagination">
                             <li class="embed-responsive"><a href="javascript:history.go(-1)">&laquo;</a></li>
                             <li class="active"><a href="../../../../index.php">1</a></li>
                             <li><a href="create.php">2</a></li>
                               <li><a href="#">3</a></li>
                               <li><a href="#">4</a></li>
                               <li><a href="#">5</a></li>
                               <li><a href="index.php">&raquo;</a></li>
                        </ul>
                   
                    
                   
        
               </div>
    
                     <div class="tab-pane" id="profile">
                           <br>
        
                              
    </div>

    <footer class="bg-success">
        <center>
            
  
    
      Atomic Pjoject &copy; AHAMAD SHARIF 107399 B10
   
 

        </center>
      
    </footer>
                    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="resource/bootstrap/jQuery 1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="resource/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>